﻿using System;
using System.Net;

namespace PlaylyfeAPI
{
    enum HttpRequestType { GET, POST, DELETE, PUT};

    interface IPlaylyfe
    {
        /// <summary>
        /// this function allows making a normal http request to playlyfe.
        /// 
        /// </summary>
        /// <param name="type">http request type</param>
        /// <param name="resource"></param>
        /// <param name="body">a json string</param>
        /// <returns>the response of the request</returns>
        HttpWebResponse makeRequest(HttpRequestType type, string resource, string body);
        
        bool initializePlayLyfeAPI(string clientKey, string clientSecret, string callbackUri);
    }

    /// <summary>
    /// This class allows to made requests to any resources on playlyfe (including those that can be accessed only when logged in).
    /// 
    /// Later on, This class should also keep track of the ouath2 token (consider putting it in a separate token manager class).
    /// Once the user authorizes the application, the auth token should be stored in a file on disk and that one should be used later on.
    /// </summary>
    internal class Playlyfe : IPlaylyfe
    {
        
        /// <summary>
        /// the constructor should take care that the user is propely logged in to playlyfe
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public Playlyfe(string username, string password){
        	throw new NotImplementedException();
        }

        public bool initializePlayLyfeAPI(string clientKey, string clientSecret, string callbackUri)
        {
            throw new NotImplementedException();
        }

        public HttpWebResponse makeRequest(HttpRequestType type, string resource, string body)
        {
            throw new NotImplementedException();
        }
    }
}