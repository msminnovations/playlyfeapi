﻿/*
 * Created by SharpDevelop.
 * User: Horia
 * Date: 06/06/2014
 * Time: 16:52
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

using RGiesecke.DllExport;

namespace dllTest
{
	/// <summary>
	/// Description of MyClass.
	/// </summary>
	public class MyClass
	{
		[DllExport("main", CallingConvention = CallingConvention.Winapi)]
		public static void main() {
			System.Console.WriteLine("Hello, World!");
		}
		
		[DllExport("add", CallingConvention = CallingConvention.Winapi)]
		public static int add(int left, int right)
		{
			return left + right;
		}
		
		[DllExport("returnOneCSharp", CallingConvention = CallingConvention.Winapi)]
		public static int returnOneCSharp()
		{
			return 21;
		}
		
		[DllExport("returnOneCSharpWithIntArgs", CallingConvention = CallingConvention.Winapi)]
		public static int returnOneCSharpWithIntArgs(int arg)
		{
			return arg;
		}
		
		
		[DllExport("returnHelloString", CallingConvention = CallingConvention.Winapi)]
		public static void returnHelloString(IntPtr arg)
		{
			string outputStr = "foo\0";
    		byte[] outputBytes = Encoding.Default.GetBytes(outputStr);
    		Marshal.Copy(outputBytes, 0, arg, outputBytes.Length);
			
		}
	}
}