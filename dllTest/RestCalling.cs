﻿/*
 * Created by SharpDevelop.
 * User: Horia
 * Date: 08/06/2014
 * Time: 15:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Net;

namespace dllTest
{
	public class RestCalling
	{
		public RestCalling()
		{
		}
		
		public static string CreateRequest(string queryString)
		{
			string urlRequest ="http://www.google.com";
//			string UrlRequest = "http://dev.virtualearth.net/REST/v1/Locations/" +
//				queryString +
//				"?output=xml" +
//				" &key=" + BingMapsKey;
			return urlRequest;
		}
		
		public static string MakeRequest(string requestUrl)
		{
			try
			{
				HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
				using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
				{
					if (response.StatusCode != HttpStatusCode.OK) {
						throw new Exception(String.Format(
							"Server error (HTTP {0}: {1}).",
							response.StatusCode,
							response.StatusDescription));
					}
                    
//					DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Response));
//					object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
//					Response jsonResponse
//						= objResponse as Response;
                    return response.ToString();
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				return null;
			}
		}
        public static void Main()
        {
            System.Console.WriteLine(MakeRequest(CreateRequest("")));
        }
	}
}
