## Task - login into playlyfe and make a successful to a protected resource
1. Fork this repository.
2. Open the dllTest.sln solution. There are various comments in the classes already in there, read them to better understand what the task is about.
3. Implement the methods in the IPlaylyfe interface in such a way that PlaylyfeMain.Main prints true.
4. Create a unit test that is relevant to the above implementation with the testing framework of your choice.

## Once Complete
1. Commit and Push your code to your new repository.
2. Send me a pull request, I will review your code and get back to you.