﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace PlayLyfe.Demo
{
    public partial class Confirm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string access_token = Convert.ToString(Request.QueryString["code"]);
            if (!string.IsNullOrEmpty(access_token))
            {
                //After you receive access token,
                //you can do anything with api with this access token

                lblMessage.Text = "You are successfully login...";
                lblMessage.ForeColor = Color.Green;
            }
            else
            {
                lblMessage.Text = "Some error occur in login...";
                lblMessage.ForeColor = Color.Red;
            }
        }
    }
}