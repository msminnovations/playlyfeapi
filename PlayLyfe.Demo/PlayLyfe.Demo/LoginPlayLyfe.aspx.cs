﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;

namespace PlayLyfe.Demo
{
    public partial class LoginPlayLyfe : System.Web.UI.Page
    {

        #region Variables

        CustomPlayLyfeClient _playLyfe;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnLoginToPlayLyfe_Click(object sender, EventArgs e)
        {

            _playLyfe = new CustomPlayLyfeClient(Properties.Settings.Default.ClientID, Properties.Settings.Default.ClientSecret);
            string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority;

            Uri returnUrl = new Uri(baseURI + "/Confirm.aspx");

            Uri redirectUrl = _playLyfe.GetServiceLoginUrl(returnUrl);
          
            Response.Redirect(redirectUrl.AbsoluteUri, false);
        }
    }
}