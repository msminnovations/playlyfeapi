﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
//using

namespace PlayLyfe.Demo
{
    public class CustomPlayLyfeClient 
    {
     
        private const string AuthorizationEP = "http://playlyfe.com/auth";
        private const string TokenEP = "http://playlyfe.com/auth/token";
        private readonly string _appId;
        private readonly string _appSecret;

        public CustomPlayLyfeClient(string appId, string appSecret)           
           
        {
            _appId = appId;
            _appSecret = appSecret;
        }


        public  Uri GetServiceLoginUrl(Uri returnUrl)
        {
            return new Uri(
                AuthorizationEP
                + "?response_type=code&client_id=" + _appId
                + "&redirect_uri=" + HttpUtility.UrlEncode( returnUrl.ToString())
                );
        }

       
    }
}